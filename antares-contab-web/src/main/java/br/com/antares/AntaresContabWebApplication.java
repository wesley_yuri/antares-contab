package br.com.antares;

import br.com.antares.entity.ProfileStatus;
import br.com.antares.repositories.ProfileStatusRespositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EntityScan("br.com.antares.entity")
@SpringBootApplication
public class AntaresContabWebApplication implements CommandLineRunner {

    @Autowired
    public ProfileStatusRespositories profStatusRepository;

    public static void main(String[] args) {
        SpringApplication.run(AntaresContabWebApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ProfileStatus pf =  new ProfileStatus();
        pf.setDescription("staus teste 1");
        pf.setStatusName("TESTE 2 " );


        System.out.println("Salvando profile status");
        profStatusRepository.save(pf);
        System.out.println("profile status Salvo com succ");
    }
}
