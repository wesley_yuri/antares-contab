package br.com.antares.entity;

import javax.persistence.*;

@Entity(name = "pro_status")
public class ProfileStatus {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "pro_status_id")
    private Long id;

    @Column(name = "pro_status_name")
    private String statusName;

    @Column(name = "pro_status_description")
    private String description;

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
