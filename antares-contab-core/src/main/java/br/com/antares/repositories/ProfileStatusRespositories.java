package br.com.antares.repositories;

import br.com.antares.entity.ProfileStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileStatusRespositories extends JpaRepository<ProfileStatus,Long> {

}
